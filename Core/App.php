<?php

namespace Core;

use Exception;

class App {

    public static function run() {
        $route = Router::getRoute();
	    $controller = $route['controller'];
	    $action     = $route['action'];
	    $params     = $route['params'];

	    try {
		    call_user_func_array([$controller, $action], $params);
	    } catch (Exception $e) {
		    print($e->getMessage());
	    }

    }
} 