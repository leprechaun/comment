<?php

namespace Core;

final class Config
{
    private static $instance;
    private $config;

    private function __clone() {}
    private function __wakeup() {}

    private function __construct()
    {
        $this->config = parse_ini_file('config.ini', true);
    }

    public static function getInstance()
    {
        if (empty(static::$instance)) {
            static::$instance = new self();
        }

        return static::$instance;
    }

    public function __get($key)
    {
        if (array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }
        return false;
    }
} 