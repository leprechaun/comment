<?php

namespace Core\Controller;

class BaseController {

    protected function render($template, $vars)
    {
        self::escapeVars($vars);

        require __DIR__ . '/../View/' . $template;
    }

    protected static function escapeVars(&$groups)
    {
        array_walk_recursive($groups, function(&$var) {
            $var = strip_tags($var, '<a>');
        });
    }
} 