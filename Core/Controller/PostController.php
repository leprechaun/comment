<?php

namespace Core\Controller;

use Core\Model\Post;

class PostController extends BaseController
{
    public function listAll()
    {
        $postRepo = new Post();
        $posts = $postRepo->findAll();

        parent::render('posts.phtml', ['posts' => $posts]);
    }

    public function view($id)
    {
        $id = intval($id);
        $postRepo = new Post();
        $post = $postRepo->find($id);

        parent::render('post.phtml', ['post' => $post]);
    }
} 