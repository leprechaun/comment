<?php

namespace Core;

use Exception;
use PDO;

class DB
{
    private static $instance;
    private $connection;

    private function __clone() {}
    private function __wakeup() {}

    private function __construct() {
        $config   = Config::getInstance();
        $host     = $config->database['host'];
        $user     = $config->database['user'];
        $pass     = $config->database['password'];
        $database = $config->database['database'];

	    try {
		    $this->connection = new PDO("mysql:host=$host;dbname=$database", $user, $pass);

		    if ( !$this->connection ) {
			    throw new Exception();
		    }
	    } catch (Exception $e) {
		    throw new Exception("Could not connect to database");
	    }

    }

    public static function getInstance()
    {
        if (empty(static::$instance)) {
            static::$instance = new self();
        }

        return static::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }
} 