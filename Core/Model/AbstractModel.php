<?php

namespace Core\Model;

use Core\DB;
use PDO;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractModel {

    /** @var PDO */
    protected $conn;
    protected $table;
    protected $fields;

    public function __construct()
    {
        /** @var DB $db */
        $db = DB::getInstance();
        $this->conn = $db->getConnection();

        $reflect = new ReflectionClass($this);
        $this->table = $reflect->getShortName();

        $this->fields = array_map(function(ReflectionProperty $field) {
            return $field->getName();
        }, $reflect->getProperties(ReflectionProperty::IS_PRIVATE));
    }

    public function findAll()
    {
        $sth = $this->conn->prepare("SELECT " . implode(', ', $this->fields) . " FROM " . $this->table);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();

        return $sth->fetchAll();
    }

    public function find($id)
    {
        $sth = $this->conn->prepare("SELECT " . implode(', ', $this->fields) . " FROM " . $this->table . " WHERE id = :id");
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute(['id' => $id]);

        return $sth->fetch();
    }

} 