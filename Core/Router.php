<?php

namespace Core;

class Router {
    private static $map = [
        '/' => 'Post:listAll',
        '/post' => 'Post:view'
    ];

    public static function getRoute()
    {
        $params = [];
        $url = parse_url($_SERVER['REQUEST_URI']);

        if (array_key_exists($url['path'], self::$map)) {
            list($controllerName, $action) = explode(':', self::$map[$url['path']]);

            if (isset($url['query'])) {
                parse_str($url['query'], $params);
            }

	        $controllerPath = __NAMESPACE__ . '\Controller\\' . $controllerName . 'Controller';
	        $controller     = new $controllerPath;

            return [
	            'controller' => $controller,
	            'action'     => $action,
                'params'     => $params
            ];
        }

        throw new \Exception('Route Not Found');
    }
}